#include "file_work.h"

#include <errno.h>
#include <stdint.h>
#include <string.h>

int file_open(const char* file_name, FILE** file) {
	if (file_name == NULL || file == NULL) return -1;
	*file = fopen(file_name, "rb");
	if (*file == NULL) return errno;
	return 0;
}

int file_open_write(const char* file_name, FILE** file) {
	if (file_name == NULL || file == NULL) return -1;
	*file = fopen(file_name, "wb");
	if (*file == NULL) return errno;
	return 0;
}

int file_close(FILE** file) {
	return fclose(*file);
}

int print_file_error(int errnum) {
	if (errnum) fprintf(stderr, "File error: %s\n", strerror(errnum));
	return errnum;
}
