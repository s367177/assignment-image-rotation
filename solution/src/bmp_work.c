#include "bmp_work.h"

static uint8_t calc_padding(struct image const *image) {
	return image->width % 4;
}

static struct bmp_header create_header(struct image const* image) {
	struct bmp_header new_header;
	new_header.bfType = FILE_HEADER_BITS;
	new_header.bfileSize = sizeof(struct bmp_header) + 3 * image->width * image->height + calc_padding(image) * image->height;
	new_header.bfReserved = 0;
	new_header.bOffBits = BITMAPINFOHEADER_SIZE + BITMAPFILEHEADER_SIZE;
	new_header.biSize = BITMAPINFOHEADER_SIZE;
	new_header.biWidth = image->width;
	new_header.biHeight = image->height;
	new_header.biPlanes = 1;
	new_header.biBitCount = BITS_PER_PIXEL;
	new_header.biCompression = 0;
	new_header.biSizeImage = image->height * (image->width + calc_padding(image));
	new_header.biXPelsPerMeter = PEXELS_PER_METER;
	new_header.biYPelsPerMeter = PEXELS_PER_METER;
	new_header.biClrUsed = 0;
	new_header.biClrImportant = 0;
	return new_header;
}

enum read_status from_bmp(FILE *in_file, struct image *image) {
	struct bmp_header header = {0};
	if (fread(&header, sizeof(struct bmp_header), 1, in_file) != 1) {
		return READ_INVALID_HEADER;
	}
	if (header.bfType != FILE_HEADER_BITS || header.biBitCount != BITS_PER_PIXEL || header.bfReserved != 0 || header.biSize != BITMAPINFOHEADER_SIZE) {
		return READ_INVALID_SIGNATURE;
	}
	uint32_t read_bits = 0;
	*image = image_create(header.biWidth, header.biHeight);
	const uint8_t bmp_padding = calc_padding(image);
	for(size_t i = 0; i < image->height; i++) {
		read_bits = fread(&(image->data[image->width * i]), sizeof(struct pixel), image->width, in_file);
		if (read_bits != image->width) {
			image_free(image);
			return READ_INVALID_BITS;
		}
		if (bmp_padding)
			if (fseek(in_file, bmp_padding, SEEK_CUR) != 0) {
				image_free(image);
				return READ_ERROR;
			}
	}
	return READ_OK;
}

enum write_status to_bmp(FILE *out_file, struct image const *image) {
	struct bmp_header header = create_header(image);
	if (fwrite(&header, sizeof(struct bmp_header), 1, out_file) != 1) {
		return WRITE_ERROR;
	}
	const uint8_t bmp_padding = calc_padding(image);
	const uint32_t garbage = 0;
	for(size_t i = 0; i < image->height; i++) {
		if (fwrite(&image->data[i * image->width], sizeof(struct pixel), image->width, out_file)==3*image->width) {
			return WRITE_ERROR;
		}
		if (bmp_padding)
			if (!fwrite(&garbage, 1, bmp_padding, out_file)) {
				return WRITE_ERROR;
			}
	}
	return WRITE_OK;
}
