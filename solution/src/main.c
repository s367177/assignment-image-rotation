#include "errors.h"
#include "file_work.h"
#include "rotating.h"

#define print_error(...) fprintf(stderr, __VA_ARGS__)

int main(int argc, char** argv) {
	if (argc != 3) {
		print_error("Invalid number of arguments\n");
		return 1;
	}
	FILE *inp_file = NULL;
	if (print_file_error(file_open(argv[1], &inp_file))) return 1;
	
	struct image inp_image = {0, 0, NULL};
	
	enum read_status read_status = from_bmp(inp_file, &inp_image);
	if (file_close(&inp_file)) {
		print_error("Input file close error!");
		image_free(&inp_image);
		return 1;
	}
	
	if (read_status) {
		print_error("%s\n", parse_read_error(read_status));
		image_free(&inp_image);
		return 1;
	}
	
	struct image out_image = image_rotate(&inp_image);
	if (!out_image.width || !out_image.height) {
		print_error("Rotation failed\n");
		image_free(&inp_image);
		image_free(&out_image);
		return 1;
	}
	FILE *out_file = NULL;
	if (print_file_error(file_open_write(argv[2], &out_file))) return 1;
	enum write_status write_status = to_bmp(out_file, &out_image);
	if (file_close(&out_file)) {
		print_error("Output file close error!");
		image_free(&inp_image);
		image_free(&out_image);
		return 1;
	}
	if (write_status) {
		print_error("%s\n", parse_write_error(write_status));
		image_free(&inp_image);
		image_free(&out_image);
		return 1;
	}
	image_free(&inp_image);
	image_free(&out_image);
	return 0;
}
