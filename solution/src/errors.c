#include "errors.h"

const char* read_error_messages[] = {
	[READ_INVALID_SIGNATURE] = "Something went wrong while reading the file. Please check the file type and try again",
	[READ_INVALID_BITS] = "The number of bits in the file seems to be incorrect. Please verify the file and try again",
	[READ_INVALID_HEADER] = "The BMP header in the file is invalid. Please ensure the file has a valid BMP header",
	[READ_NO_DATA] = "No data was found in the file. Please make sure the file contains data and try again",
	[READ_CLOSING_ERROR] = "The file wasn't closed properly. Please ensure the file is closed correctly before reading",
	[READ_ERROR] = "An error with reading this file"
};

const char* write_error_messages[] = {
	[WRITE_ERROR] = "Something went wrong while writing to the file. Please check your write permissions and try again"
};

const char* parse_read_error(enum read_status status) {
	return read_error_messages[status];
}

const char* parse_write_error(enum write_status status) {
	return write_error_messages[status];
}
