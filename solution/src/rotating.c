#include "rotating.h"
#include <stdlib.h>


struct image image_rotate(struct image const *image) {
	struct image image_result = image_create(image->height, image->width);
	uint32_t h = image_result.height;
	uint32_t w = image_result.width;
	for (size_t i = 0; i < w; ++i)
		for (size_t j = 0; j < h; ++j)
			image_result.data[(w - 1 - i) + j*w] = image->data[i*h + j];
	return image_result;
}
