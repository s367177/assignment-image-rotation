#include "image.h"
#include <stdlib.h>


struct image image_create(uint32_t width, uint32_t height) {
	struct pixel *data = malloc(sizeof(struct pixel) * width * height);
	if (!data) return (struct image) {.width = 0, .height = 0, .data = NULL};
	return (struct image) {.width = width, .height = height, .data = data};
}

void image_free(struct image *image) {
	if (!image) return;
	if (image->data) free(image->data);
}
