#ifndef FILE_WORK_H367177
#define FILE_WORK_H367177

#include <stdio.h>

int file_open(const char* file_name, FILE** file);
int file_open_write(const char* file_name, FILE** file);
int file_close(FILE** file);
int print_file_error(int errnum);

#endif
