#ifndef IMAGE_H367177
#define IMAGE_H367177

#include <stdint.h>

struct pixel {
	uint8_t b, g, r;
};

struct image {
	uint32_t width, height;
	struct pixel *data;
};

struct image image_create(uint32_t width, uint32_t height);
void image_free(struct image *image);

#endif
