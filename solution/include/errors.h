#ifndef ERRORS_H367177
#define ERRORS_H367177

#include "bmp_work.h"

const char* parse_read_error(enum read_status status);
const char* parse_write_error(enum write_status status);

#endif
