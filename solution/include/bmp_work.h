#ifndef BMP_WORK_H367177
#define BMP_WORK_H367177

#include "image.h"
#include <stdio.h>

#define FILE_HEADER_BITS 0x4d42
#define BITS_PER_PIXEL 24
#define BITMAPINFOHEADER_SIZE 40
#define BITMAPFILEHEADER_SIZE 14
#define PEXELS_PER_METER 2834

struct __attribute__((packed)) bmp_header
{
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t  biClrImportant;
};

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_NO_DATA,
	READ_CLOSING_ERROR,
	READ_ERROR
};

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum read_status from_bmp(FILE *in_file, struct image *image);
enum write_status to_bmp(FILE *out_file, struct image const *image);

#endif
