#ifndef ROTATING_H367177
#define ROTATING_H367177

#include "image.h"

struct image image_rotate(struct image const *image);

#endif
